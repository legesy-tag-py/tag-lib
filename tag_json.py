import json


def json_open(path_to_file) -> dict:
    """
    read a csv file and return contests as a list

    :param path_to_file: path
    :return: dict
    """
    with open(path_to_file, mode='r') as json_file:
        tag_list = json.load(json_file)
        json_file.close()
        return tag_list


def json_write(path_to_file, tag_list: dict):
    """
    write directory to file

    :param path_to_file:
    :param tag_list: dict
    """
    with open(path_to_file, mode='w') as json_file:
        json.dump(tag_list, json_file)
        json_file.close()


def list_tags(tag_dict: dict) -> list:
    """
    takes a directory and returns a list of keys

    :param tag_dict: directory
    :return: list
    """
    return [[*tag_dict.keys()]]


def remove_duplicates_from_list(list_of_items: list) -> list:
    """
    takes a list and removes all duplicates

    :param list_of_items: list
    :return: list
    """
    return list(dict.fromkeys(list_of_items))


def list_items_in_tag(tag_dict: dict, tag) -> list:
    """
    add all items from the tags og tag to a list

    :param tag_dict: dict
    :param tag:
    :return: list
    """
    if type(tag) is list:
        return remove_duplicates_from_list([[*tag_dict[item]] for item in tag])
    else:
        return [[*tag_dict[tag]]]


def is_tag(tag_dict: dict, tag) -> bool:
    """
    checks if tag is created

    :param tag_dict: dict
    :param tag:
    :return: bool
    """
    return bool(tag in tag_dict)


def search_for_file(tag_dict: dict, file: str) -> list:
    """
    searches for file in the values

    :param tag_dict: dict
    :param file: str
    :return: list
    """
    tags_appended = []
    for tags, var in tag_dict.items():
        for item in var:
            if item == file:
                tags_appended.append(tags)
                break
    return tags_appended


def contains_file(tag_dict: dict, tag: str, file: str) -> bool:
    """
    checks if the result from search_for_file contains the tag and returns a bool value

    :param tag_dict: dict
    :param tag: str
    :param file:str
    :return: bool
    """
    return bool(tag in search_for_file(tag_dict, file))


def create_tag(tag_dict: dict, tag: str):
    """
    check before you create a new tag

    :param tag_dict: dict
    :param tag: str
    """
    if is_tag(tag_dict, tag):
        print(f'the tag ({tag}) already exists')
    else:
        tag_dict[tag] = []


def if_not_tag_make_it(tag_dict: dict, tag: str):
    """
    checks if item exists and asks the user about creating it if item don't exist

    :param tag_dict:dict
    :param tag: str
    """
    if is_tag(tag_dict, tag):
        return
    elif input(f'\nthe tag {tag} is not found in the tag_list du you want to create it? (y/N)') != 'y':
        print('closing')
        exit()
    else:
        create_tag(tag_dict, tag)


def remove_tag(tag_dict: dict, tag: str):
    """
    removes tag from list and moves any tagged files to untagged

    :param tag_dict: dict
    :param tag: str
    """
    if not is_tag(tag_dict, tag):
        print(f'\nthe tag: {tag}, du not exist')
    else:
        loop_list = [*tag_dict[tag]]
        for item in loop_list:
            untag_file(tag_dict, tag, item)

            if contains_file(tag_dict, "untagged", item):
                print(f'{item} not tagged elasware, {item} now tagged to "untagged"')
                tag_file(tag_dict, 'untagged', item)

        del tag_dict[tag]


def tag_file(tag_dict: dict, tag: str, file: str):
    """
    assigning a tag to a file

    :param tag_dict: dict
    :param tag: str
    :param file: str
    """
    if_not_tag_make_it(tag_dict, tag)
    if contains_file(tag_dict, tag, file):
        return f'{file} already tagged to {tag}'
    else:
        if contains_file(tag_dict, 'untagged', file):
            tag_dict['untagged'].remove(file)
        tag_dict[tag].append(file)
        return f'{file} now tagged to {tag}'


def untag_file(tag_dict: dict, tag: str, file: str):
    """
    remove file from tag if not tagged else were tag to untagged

    :param tag_dict: dict
    :param tag: str
    :param file: str
    """
    if not contains_file(tag_dict, tag, file):
        print(f'the file {file} are\'t tagged at {tag}')
    else:
        tag_dict[tag].remove(file)

    if not contains_file(tag_dict, 'untagged', file):
        tag_file(tag_dict, 'untagged', file)
